package com.example.demo.exception;


public class TechnicalException extends RuntimeException {
    public TechnicalException() {
        super();
    }
    public TechnicalException(String message, Throwable cause) {
        super(message, cause);
    }
    public TechnicalException(String message) {
        super(message);
    }
    public TechnicalException(Throwable cause) {
        super(cause);
    }
}
