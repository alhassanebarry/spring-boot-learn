package com.example.demo.service;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.example.demo.exception.TechnicalException;
import com.example.demo.repository.UserRepository;
import com.example.demo.util.ValueMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
public class UserService {

    UserRepository userRepository;

    public List<UserDTO> getUsers( Pageable paging ) {
        log.info("[UserService][getUsers] started kkkkk....");
        List<UserDTO> userDTOS;

        Page<User> users = userRepository.findAll( paging );

        if(users.isEmpty()){
            userDTOS= Collections.emptyList();
        } else{
            userDTOS = users
                    .stream()
                    .map(ValueMapper::convertToDTO)
                    .collect(Collectors.toList());
        }
        log.info("[UserService][getUsers] ended ....");

        return userDTOS;



    }
    @Cacheable(value = "user")
    public UserDTO findById(Integer id) {
        UserDTO userDTO = null;
        log.info("[UserService][findUserById] started....");
        try{
            User user = userRepository.findById(id).orElseThrow(()->new TechnicalException("with id "+id));
            userDTO = ValueMapper.convertToDTO(user);
            log.debug("[UserService][findUserById] retrieving user from database for id {} {}", id, ValueMapper.jsonAsString(userDTO));

        } catch (Exception ex) {
            log.error("[UserService][findUserById] error.... {}", ex.getMessage());
            throw new TechnicalException("Exception occurred while fetch user from Database "+ex.getMessage());
        }

        log.info("[UserService][findUserById] ended....");
        return userDTO;

    }
}
