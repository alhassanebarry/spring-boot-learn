package com.example.demo.controller;

import com.example.demo.dto.APIResponse;
import com.example.demo.dto.UserDTO;
import com.example.demo.service.UserService;
import com.example.demo.util.ValueMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.awt.image.BufferedImage;
import java.util.List;

@RestController
@RequestMapping(value = "users")
@AllArgsConstructor
@Slf4j
public class UserController {

    private static final String SUCCESS = "success";
    UserService userService;

    @GetMapping("/{id}")
    ResponseEntity<APIResponse> findById(
            @PathVariable Integer id
    ){

        UserDTO user = userService.findById(id);
        APIResponse<UserDTO> responseDTO = APIResponse
                .<UserDTO>builder()
                .status(SUCCESS)
                .results(user)
                .build();


        log.info("[User Controller][Find By Id] Response {}", ValueMapper.jsonAsString(responseDTO));

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping
    ResponseEntity<APIResponse> getUsers(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size
    ){
        Pageable paging = PageRequest.of(page, size, Sort.by("name").descending().and(Sort.by("department")));

        List<UserDTO> users = userService.getUsers(paging);
        APIResponse<List<UserDTO>> responseDTO = APIResponse
                .<List<UserDTO>>builder()
                .status(SUCCESS)
                .results(users)
                .build();


        log.info("[UserController][getUsers] response {}", ValueMapper.jsonAsString(responseDTO));

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }


}
