package com.example.demo.dto;

import lombok.*;


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class UserDTO {
    private int id;
    private String name;
    private String department;
    private double salary;


}