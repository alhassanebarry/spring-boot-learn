package com.example.demo.util;

import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ValueMapper {

    public static User convertToEntity(UserDTO userDTO) {
        return User.builder()
                .id(userDTO.getId())
                .department(userDTO.getDepartment())
                .name(userDTO.getName())
                .salary(userDTO.getSalary())
                .build();

    }

    public static UserDTO convertToDTO(User user){
        return UserDTO.builder()
                .id(user.getId())
                .department(user.getDepartment())
                .name(user.getName())
                .salary(user.getSalary())
                .build();
    }


    public static String jsonAsString(Object obj){
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
