package com.example.demo.handler;

import com.example.demo.dto.APIResponse;
import com.example.demo.dto.ErrorDTO;
import com.example.demo.exception.TechnicalException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.EMPTY;

@RestControllerAdvice
public class UserServiceHandler {

    public static final String FAILED = "FAILED";

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public APIResponse<?> handleMethodArgumentException(MethodArgumentNotValidException exception) {
        APIResponse<?> apiResponse = new APIResponse<>();
        List<ErrorDTO> errors = new ArrayList<>();
        exception.getBindingResult().getFieldErrors().forEach(
                fieldError -> errors.add(new ErrorDTO(fieldError.getField(), fieldError.getDefaultMessage()))
        );
        apiResponse.setStatus(FAILED);
        apiResponse.setErrors(errors);
        return apiResponse;

    }

    @ExceptionHandler(TechnicalException.class)
    public APIResponse<?> handleTechnicalException(TechnicalException exception){
        APIResponse<?> apiResponse = new APIResponse<>();
        apiResponse.setStatus(FAILED);
        apiResponse.setErrors(Collections.singletonList(new ErrorDTO(EMPTY,exception.getMessage())));
        return apiResponse;
    }
}
